var express = require('express');
var app = express();
var main = require('./app/main');

module.exports = app;

require('./config/routes')(app);

/*var mongojs = require('mongojs');
var db = mongojs('contactlist',['contactlist'])*/

var server_port = process.env.PORT || 3000;

app.set('view engine', 'jade');
app.set('views', __dirname + '/app/views/layouts');
app.use(express.static(__dirname + '../app'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/')); //include jquery
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist/')); //include bootstrap
//app.set('/public', __dirname + '/public/');
app.use('/styles', express.static(__dirname + '/public/css/')); //include styles
app.use('/scripts', express.static(__dirname + '/public/css/')); //include scripts



app.listen(server_port, function () {
  console.log('Listening on port ', server_port)
})