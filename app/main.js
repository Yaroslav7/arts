var requirejs = require('requirejs');

requirejs.config({
    paths:{
        jquery: "./node_modules/jquery/dist/jquery",
        bootstrap: "./node_modules/bootstrap/dist/js/bootstrap"
    }
});