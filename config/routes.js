module.exports = function (app) {
    app.get('/', function (req, res) {
        res.render('index', {type: 'home'});
    });

    app.get('/admin', function (req, res) {
        res.render('admin', {type: 'admin'});
    });

}

